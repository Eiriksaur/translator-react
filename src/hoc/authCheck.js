import React from 'react';
import { Redirect } from 'react-router-dom';

const authCheck = Component => props => {
    if(props.username){
        return <Component {...props}/>
    }else {
        return <Redirect to='/login'/>
    }
}

export default authCheck;