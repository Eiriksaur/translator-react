import React from 'react'

const MyPage = (props) => {

    const translationList = props.sentTranslationList.map((translation, idx) => {
        return (
          <div key={idx}>
              {translation}
          </div>
        )
    })
    return (
        <div>
            <h2>Your translations</h2>
            <div>
                {translationList}
            </div>
        </div>
    )

}

export default MyPage;