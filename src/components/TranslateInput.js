import React, {useState} from 'react'

const TranslateInput = (props) =>{
    
    const [inputString, setInputString] = useState('')

    const buttonClicked = () => {
        props.sendInputChanges(inputString)
    }

    const inputChanged = (e) =>{
        setInputString(e.target.value)
    }

    return(
        <div>
            <input placeholder="Write your word here" onChange={inputChanged}></input>
            <button onClick={buttonClicked}>Translate!</button>
        </div>
    )

}

export default TranslateInput;