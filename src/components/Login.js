import React,{useState} from 'react'
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField'
import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import AccountCircle from '@material-ui/icons/AccountCircle';
import Grid from '@material-ui/core/Grid'
import {setStorage} from '../utils/storage';
import {Redirect} from 'react-router-dom'



const useStyles = makeStyles((theme) => ({
    bgWhite:{
        backgroundColor: 'white'
    },
    central: {
        display: "flex", 
        justifyContent: "center", 
        alignItems: "center"
    }, 
    root: {
        minWidth: 150,
        maxWidth: 800,
        minHeight: 150,
        maxHeight: 150,
        margin: 'auto',
        padding: 80,
        backgroundColor: 'linen'
      },
      bullet: {
        display: 'inline-block',
        margin: '0 2px',
        transform: 'scale(0.8)',
      },
      title: {
        fontSize: 14,
      },
      pos: {
        marginBottom: 12,
      },
  }));

const Login = (props) =>{

    const [username, setUsername] = useState('');

    const handleUsernameChange = (event) =>{
        setUsername(event.target.value);
    }

    const onLoginClicked = () => {
        setStorage('name', {
            username,
        });
        props.sendNameChange();
    }

    const classes = useStyles()
    return(
        <div>
            {props.logInn && <Redirect to="/translate"></Redirect>}
            <Card className={classes.root} elevation={3} variant='outlined'>
                <CardContent>
                    <Grid container spacing={1} alignItems="center" className={classes.central} >
                        <Grid item>
                            <AccountCircle />
                        </Grid>
                        <Grid item>
                            <TextField className={classes.bgWhite} onChange={handleUsernameChange} variant='outlined' size='small' placeholder='Enter username...'/>  
                        </Grid>
                    </Grid>
                </CardContent>
                <Button onClick={onLoginClicked} variant='contained' color='secondary'>Login</Button>
            </Card>
        </div>
    )
}

export default Login;