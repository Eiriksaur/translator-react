import React from 'react'
import './Styles.css'



const TranslateWindow = (props) =>{
    
    const myStyle = {
        maxWidth: 50,
        maxHeight: 50,
        display: 'inline'
    }

    const signList = props.input.map((imgSrc, idx) => {
        if(imgSrc === " "){
            console.log("mellomrom")
            return(<div key={idx}>
                    <p id="blank-space"></p>
                </div>
                )
        } else{
            return (
            <div key={idx}>
                <img style={myStyle} src={imgSrc} alt="This is signlanguage"></img>
            </div>
            )
        }
    })

    return(
        <div className={myStyle}>
            <h3>Translation</h3>
            <div id="signcontainer">{signList}</div>
        </div>
    )

}

export default TranslateWindow;