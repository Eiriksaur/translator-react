import {createStore, combineReducers} from 'redux';
import userReducer from './reducers/user.reducers.js';

const rootReducers = combineReducers({
    username: userReducer
});

const store = createStore(
    rootReducers,
    window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
);

export default store;
