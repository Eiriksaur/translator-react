//Types
export const ACTION_SET_USERNAME ='ACTION_SET_USERNAME';

//Actions
export const setUsernameAction = (username = "") => ({
    type: ACTION_SET_USERNAME,
    payload: username
});