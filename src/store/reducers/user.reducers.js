import {ACTION_SET_USERNAME} from '../actions/user.actions.js';

const userReducer = (state = "", action) => {
    switch (action.type){
        case ACTION_SET_USERNAME:
            return action.payload;
        default:
            return state;
    }
};

export default userReducer;