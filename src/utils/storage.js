export const setStorage = (key, value) => {
    localStorage.setItem(key, JSON.stringify(value));
}

export const getStorage = (key) => {
    const storedValue = localStorage.getItem(key);
    if(storedValue) return JSON.parse(storedValue);
    return false;
}

export const storeTranslation = (value) => {
    let transList = getStoredTranslations();
    let altTransList = [];
    if(!transList){
        altTransList.push(value)
        localStorage.setItem('Translations', JSON.stringify(altTransList))
    } else if(transList.length >= 10){
        altTransList.push(...transList)
        altTransList.shift()
        altTransList.push(value)
        localStorage.setItem('Translations', JSON.stringify(altTransList))
    } else {
        altTransList.push(...transList)
        altTransList.push(value)
        localStorage.setItem('Translations', JSON.stringify(altTransList))
    }
}

export const getStoredTranslations = () =>{
    const storedTranslations = localStorage.getItem('Translations');
    if(storedTranslations) return JSON.parse(storedTranslations);
    return [];
}

export const deleteStorageItem = (key) =>{
    localStorage.removeItem(key)
    return !getStorage(key);
}