import React, {useState, useEffect} from 'react'
import Login from '../components/Login'
import {getStorage} from '../utils/storage'
import {useDispatch } from 'react-redux';
import {setUsernameAction} from '../store/actions/user.actions.js'

const LoginView =(props) =>{

    const [isLoggedIn, setIsLoggedIn] = useState(false);
    const dispatch = useDispatch();
    
    useEffect(()=>{
        setIsLoggedIn(getStorage('name'))
    },[])
    
    const handleNameChange = () =>{
        dispatch(setUsernameAction(getStorage('name').username))
        setIsLoggedIn(true);
    }

    let childProps = {
        sendNameChange: handleNameChange,
        logInn: isLoggedIn
    }

    return(
        <div>
            <Login {...childProps}/>
        </div>
    )
}

export default LoginView