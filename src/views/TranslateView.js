import React, {useState} from 'react'
import TranslateWindow from '../components/TranslateWindow'
import TranslateInput from '../components/TranslateInput'
import {storeTranslation} from '../utils/storage'
import authCheck from '../hoc/authCheck'
import A from '../assets/a.png'
import B from '../assets/b.png'
import C from '../assets/c.png'
import D from '../assets/d.png'
import E from '../assets/e.png'
import F from '../assets/f.png'
import G from '../assets/g.png'
import H from '../assets/h.png'
import I from '../assets/i.png'
import J from '../assets/j.png'
import K from '../assets/k.png'
import L from '../assets/l.png'
import M from '../assets/m.png'
import N from '../assets/n.png'
import O from '../assets/o.png'
import P from '../assets/p.png'
import Q from '../assets/q.png'
import R from '../assets/r.png'
import S from '../assets/s.png'
import T from '../assets/t.png'
import U from '../assets/u.png'
import V from '../assets/v.png'
import W from '../assets/w.png'
import X from '../assets/x.png'
import Y from '../assets/y.png'
import Z from '../assets/z.png'

const TranslateView = (props) =>{

    const alphabet = [A,B,C,D,E,F,G,H,I,J,K,L,M,N,O,P,Q,R,S,T,U,V,W,X,Y,Z];
    
    const [selectedSigns, setSelectedSigns] = useState([])

    const cleanString = (inputString) =>{
        let inputLetters = inputString.split('');
        let returnList = []
        for(let i = 0; i < inputLetters.length; i++){
            if(inputLetters[i] === " ") returnList.push(" ");
            if(inputLetters[i].toUpperCase().charCodeAt(0) > 90 || inputLetters[i].toUpperCase().charCodeAt(0) < 65){
                continue;
            } else {
                returnList.push(inputLetters[i]);
            }
        }
        return returnList;
    }

    const updateOutput = (inputString) => {
        let textString = cleanString(inputString)
        if(textString.length > 1) storeTranslation(textString);
        let signList = []
        for(let i = 0; i < textString.length; i++){
            if(textString[i] === " ") signList.push(" ");
            if(textString[i].toUpperCase().charCodeAt(0) > 90 || textString[i].toUpperCase().charCodeAt(0) < 65){
                continue;
            } else {
                signList.push(alphabet[textString[i].toUpperCase().charCodeAt(0)-65]);
            }
        }
        setSelectedSigns(signList);
    }

    let childProps = {
        input: selectedSigns,
    }

    return(
        <div>
            <TranslateInput sendInputChanges={updateOutput}/>
            <TranslateWindow {...childProps}/>
        </div>
    )
}

export default authCheck(TranslateView)