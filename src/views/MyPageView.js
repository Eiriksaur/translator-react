import React, { useState, useEffect } from 'react';
import {getStoredTranslations} from '../utils/storage';
import MyPage from '../components/MyPage';
import authCheck from '../hoc/authCheck'

const MyPageView = (props) => {

    const [storedTranslations, setStoredTranslations] = useState([]);

    useEffect(()=>{
        setStoredTranslations(getStoredTranslations());
    },[]);

    return(
        <div>
            <MyPage sentTranslationList={storedTranslations}/>
        </div>
    )
}

export default authCheck(MyPageView);