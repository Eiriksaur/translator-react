import React, {useEffect} from 'react';
import './App.css';
import LoginView from './views/LoginView'
import AppBar from '@material-ui/core/AppBar'
import Typography from '@material-ui/core/Typography';
import Toolbar from '@material-ui/core/Toolbar';
import AccountCircle from '@material-ui/icons/AccountCircle';
import {getStorage, deleteStorageItem} from './utils/storage';
import { makeStyles } from '@material-ui/core/styles';
import PanTool from '@material-ui/icons/PanTool';
import {BrowserRouter as Router, Route, Switch, Link, Redirect} from 'react-router-dom'
import TranslateView from './views/TranslateView'
import MyPageView from './views/MyPageView'
import './components/Styles.css'
import { useSelector, useDispatch } from 'react-redux';
import {setUsernameAction} from './store/actions/user.actions.js'

const useStyles = makeStyles((theme) => ({
  title: {
    flexGrow: 1,
  }
}));

function App() {

  const classes = useStyles()

  const username = useSelector(state => state.username);
  const dispatch = useDispatch();

  useEffect(()=>{
    let tempName = getStorage('name').username
    if(tempName){
      dispatch(setUsernameAction(tempName))
    }
  },[dispatch])

  const logOut = () =>{
    dispatch(setUsernameAction(""))
    deleteStorageItem('name');
    deleteStorageItem('Translations');
  }


  return (
    <Router>
      <div className="App">
        <AppBar position='static'>
          <Toolbar>
              <Typography className={classes.title} variant='h4'>
                Lost in translation <PanTool/>
              </Typography>
                {username && <Link to="/mypage" id="nameLink"><h4>{username}</h4></Link>}
                <AccountCircle/>
                {username && <Link to="/" id="nameLink" onClick={logOut}><h4>Log out</h4></Link>}
          </Toolbar>
        </AppBar>
        <Switch>
          <Route exact path="/">
            <Redirect to="/login"/>
          </Route>
          <Route path="/login">
            <LoginView username={username}></LoginView>
          </Route>
          <Route path="/translate" >
            <TranslateView username={username}></TranslateView>
          </Route>
          <Route path="/mypage">
            <MyPageView username={username}></MyPageView>
          </Route>
          <Route path="/"><Redirect to="/login"/></Route>
        </Switch>
      </div>
    </Router>
  );
}

export default App;
